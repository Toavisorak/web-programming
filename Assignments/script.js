"use strict";

(function(){
    let x = 1;
    if(x === 1){
        let x = 2;
        console.log("inside scope");
        console.log(`x = ${x}`);
    }

        console.log("outside scope");
        console.log(`x = ${x}`);

    var y = 1;
    if( y === 1){
        var y = 2;
        console.log("inside scope");
        console.log(`x = ${y}`);
    }

        console.log("outside scope");
        console.log(`x = ${y}`);

    const c = 0;

    console.debug(c);

    console.error(`ERROR 404: Error not found`);

    console.log("false == 'false'", false == 'false')
    console.log("false === 'false'", false === 'false')

    console.log("false =='0",false == '0')
    console.log("false ==='0",false === '0')
})();