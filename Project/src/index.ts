import { Item } from "./item";
import { Settlement, Tavern, Character } from "./settlement";

export class Main{
    constructor(){
        new Item();
        new Settlement();
        new Tavern();
        new Character();
    }
}

new Main();