class Effect {
    public name: string;
    public description: string;
    public color: string;
    private effectType: number;
    private effectDescription: string;

    constructor(name: string, description: string, color: string, effectType: number, effectDescription: string){
        this.name = name;
        this.description = description;
        this.color = color;
        this.effectType = effectType;
        this.effectDescription = effectDescription;
    }

    public effect(amount: number): string{
        let effect: string;

        //1. Additive
        //2. Multiplicative
        //3. Additive x10
        //4. Multiplicative x10
        //0. Other (no number)

        switch(this.effectType){
            case 1: effect = ((amount > 0) ? "+" : "")  + amount + " " + this.effectDescription;
                    break;
            case 2: effect = "x" + amount + " " + this.effectDescription;
                    break;
            case 3: effect = ((amount > 0) ? "+" : "") + amount * 10 + " " + this.effectDescription;
                     break;
            case 4: effect = "x" + amount * 10 + " " + this.effectDescription;
                    break;
            case 0: effect = this.effectDescription + (amount >= 0? "" : "This item is also possesed by an entity that may or may not be hostile.");
                    break;
            default: effect = "";
        }

        return effect;
    }
}

class Rarity {
    public rarity: string;
    public color: string;
    public modifier: number;

    constructor(rarity: string, color: string, modifier: number){
        this.rarity = rarity;
        this.color = color;
        this.modifier = modifier;
    }
}

class ItemType {
    public name: string;

    constructor(name: string){
        this.name = name;
    }
}

export class Item{
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D;
    private centerX: number;
    private centerY: number;
    private currentX: number;
    private currentY: number;

    private fillColor = "black";
    private stroke = 2;
    private width: number;
    private height: number;
    private rarity: Rarity;
    private effect: Effect;
    private scale: number;
    private itemType: ItemType;

    private effects = [ 
        new Effect("Vitality","affects the health of whoever is using it","#990000",1,"health points per level"), 
        new Effect("Strength", "affects the strength of whoever is using it ", "#ff9933", 1, "strength modifier"),
        new Effect("Speed", "affects the speed of whoever is using it", "lime", 3, "movement speed"),
        new Effect("Holding","can be used to open an extradimensional space to store items in","tan",0,"A storage dimension that can hold 2 cubic meters of stuff"),
        new Effect("Water Breathing", "grants the user the ability to breath underwater", "blue", 0, "Water Breathing"),
        new Effect("Defense", "magically affects the user's defensive ability", "silver", 1, "to your Armor Class"),
        new Effect("Shielding", "provides a temporary shield that affects the users health in battle", "teal", 1, "temporary hitpoints per player level in combat")
    ];

    private rarities = [
        new Rarity("Legendary", "#ffe866", 4),
        new Rarity("Golden", "gold", 3),
        new Rarity("Epic", "violet", 3),
        new Rarity("Lucky", "#b3ffd9", 2),
        new Rarity("Rare", "#9999ff", 2),
        new Rarity("Common", "whitesmoke", 1),
        new Rarity("Mundane", "#cccccc", 1),
        new Rarity("Silver", "silver", 2),
        new Rarity("Wooden", "#ffcc99", -1),
        new Rarity("Broken", "#ffffcc", -2),
        new Rarity("Old", "gray", 1),
        new Rarity("Cursed", "#666699", -3),
        new Rarity("Evil", "#262626", -4)
    ];

    private itemTypes = [
        new ItemType("sword"),
        new ItemType("ring"),
        new ItemType("amulet"),
        new ItemType("shield")
    ];

    constructor() {
        this.canvas = document.getElementById("canvas") as HTMLCanvasElement;
        this.ctx = this.canvas.getContext("2d");
        this.width = this.canvas.width;
        this.height = this.canvas.height;
        this.centerX = this.width / 2;
        this.centerY = this.height / 2;
        this.currentX = this.centerX;
        this.currentY = this.centerY;
        this.ctx.strokeStyle = this.fillColor;
        this.ctx.lineWidth = this.stroke;

        this.generateOptions();

        (document.getElementById("generateItem") as HTMLInputElement).addEventListener("click", ()=>{
            this.generate();
        });
    }

    private generateOptions(){
        let rarities = document.getElementById("rarities");
        let effects = document.getElementById("effects");
        let items = document.getElementById("items");

        for(let i = 0; i < this.rarities.length; i++){
            let temp = "<option value='" + i +"'>" + this.rarities[i].rarity + "</option>";
            rarities.innerHTML += temp;
        }

        for(let i = 0; i < this.effects.length; i++){
            let temp = "<option value='" + i +"'>" + this.effects[i].name + "</option>";
            effects.innerHTML += temp;
        }

        for(let i = 0; i < this.itemTypes.length; i++){
            let temp = "<option value='" + i +"'>" + this.itemTypes[i].name + "</option>";
            items.innerHTML += temp;
        }
    }

    public getRarity(): Rarity{
        let rarity: Rarity;

        let num: number;
        let which = document.getElementById("rarities") as HTMLSelectElement;
        if(+which.value === -1) num = Math.floor(Math.random() * this.rarities.length)
        else num = +which.value;

        rarity = this.rarities[num];

        return rarity;
    }

    public getEffect(): Effect{
        let effect: Effect;
        
        let num: number;
        let which = document.getElementById("effects") as HTMLSelectElement;
        if(+which.value === -1) num = Math.floor(Math.random() * this.effects.length)
        else num = +which.value;

        effect = this.effects[num];
        return effect;
    }


    private drawAmulet(){
        this.scale = this.height / 10;
        this.ctx.lineWidth = 2;
        
        this.currentX = this.centerX;
        this.currentY = this.centerY;

        //Amulet body
        this.ctx.fillStyle = this.rarity.color;
        this.drawCircle(this.centerX, this.centerY, this.scale);

        //Amulet gem
        this.ctx.fillStyle = this.effect.color;
        this.drawCircle(this.centerX, this.centerY, this.scale / 2);

        //Amulet chord
        this.ctx.lineWidth = 2;
        this.ctx.beginPath();
        this.ctx.moveTo(this.centerX, this.centerY - this.scale);
        this.ctx.quadraticCurveTo(this.centerX + this.scale, this.centerY -this.scale * 4, this.centerX, this.centerY - this.scale * 4);
        this.ctx.stroke();
        this.ctx.beginPath();
        this.ctx.moveTo(this.centerX, this.centerY - this.scale);
        this.ctx.quadraticCurveTo(this.centerX - this.scale, this.centerY -this.scale * 4, this.centerX, this.centerY - this.scale * 4);
        this.ctx.stroke();

    }

    private drawSword(){
        this.scale = this.height / 10;
        this.ctx.lineWidth = 2;

        this.currentX = this.centerX;
        this.currentY = this.height - this.scale;

        this.currentX += this.scale;
        this.currentY -= this.scale;

        this.ctx.strokeStyle = this.fillColor;
        this.fillColor = "black";

        //Handle
        this.drawLine(this.centerX + this.scale / 4,this.height - this.scale * 2,this.centerX + this.scale / 4,this.currentY);
        this.drawLine(this.centerX - this.scale / 4,this.height - this.scale * 2,this.centerX - this.scale / 4,this.currentY);
        this.ctx.fillStyle = this.rarity.color;
        this.drawRectangle(this.centerX - this.scale / 4, this.height - this.scale * 2 - this.scale, this.scale / 2, this.height - this.currentY);

        //Main Blade
        this.fillColor = "black";
        this.drawLine(this.centerX + this.scale,this.currentY -= this.scale,this.centerX - this.scale,this.currentY);
        this.drawLine(this.centerX + this.scale / 2,this.currentY,this.centerX + this.scale / 2,this.currentY - this.scale * 5);
        this.drawLine(this.centerX - this.scale / 2,this.currentY,this.centerX - this.scale / 2,this.currentY -= this.scale * 5);
        this.ctx.fillStyle = this.rarity.color;
        this.drawRectangle(this.centerX - this.scale / 2, this.currentY, this.scale, this.scale * 5);

        //Sword Tip
        this.ctx.strokeStyle = this.rarity.color;
        this.drawTriangle(this.centerX - this.scale / 2, this.currentY, this.centerX, this.currentY - this.scale, this.centerX + this.scale / 2, this.currentY);
        this.ctx.strokeStyle = "black";
        this.drawLine(this.centerX - this.scale / 2,this.currentY,this.centerX,this.currentY - this.scale);
        this.drawLine(this.centerX + this.scale / 2,this.currentY,this.centerX,this.currentY -= this.scale);
        this.drawLine(this.centerX,this.height - this.scale * 3, this.centerX, this.currentY);

        //Hilt
        this.ctx.fillStyle = this.effect.color;
        this.drawCircle(this.centerX, this.height - this.scale, this.scale / 2);
    }

    private drawRing(){
        this.scale = this.height / 10;
        this.ctx.lineWidth = 2;
        
        this.fillColor = "black";
        this.ctx.strokeStyle = this.fillColor;

        //Outer ring
        this.ctx.fillStyle = this.rarity.color;
        this.drawCircle(this.centerX, this.centerY, this.scale);

        //InnerRing
        this.ctx.fillStyle = "white";
        this.drawCircle(this.centerX, this.centerY, this.scale / 2);

        //Gem
        this.ctx.fillStyle = this.effect.color;
        this.drawCircle(this.centerX, this.centerY - this.scale, this.scale / 2);
    }

    private drawShield(){
        this.scale = this.height / 10;
        this.ctx.lineWidth = 2;
        
        this.fillColor = "black";
        this.ctx.strokeStyle = this.fillColor;

        //Outer shape
        this.ctx.fillStyle = this.rarity.color;
        this.ctx.lineWidth = 4;
        this.ctx.beginPath();
        this.ctx.moveTo(this.centerX, this.centerY - this.scale * 2)
        this.ctx.quadraticCurveTo(this.centerX + this.scale / 3, this.centerY - this.scale / 3,this.centerX + this.scale * 2,this.centerY - this.scale);
        this.ctx.lineTo(this.centerX, this.centerY + this.scale *3);
        this.ctx.lineTo(this.centerX - this.scale * 2, this.centerY - this.scale);
        this.ctx.quadraticCurveTo(this.centerX - this.scale / 3, this.centerY - this.scale / 3, this.centerX, this.centerY - this.scale * 2);
        this.ctx.fill();
        this.ctx.stroke();
        this.ctx.closePath();

        //Gems
        this.ctx.fillStyle = this.effect.color;
        this.drawCircle(this.centerX, this.centerY, this.scale / 4);
        this.drawCircle(this.centerX + this.scale, this.centerY - this.scale / 3, this.scale / 5);
        this.drawCircle(this.centerX - this.scale, this.centerY - this.scale / 3, this.scale / 5);

    }

    private drawLine(startX: number, startY: number, endX: number, endY: number): void{
        this.ctx.beginPath();
        this.ctx.moveTo(startX,startY);
        this.ctx.lineTo(endX,endY);
        this.ctx.stroke();
        this.ctx.closePath();
    }

    private drawCircle(x: number, y: number, r: number){
        this.ctx.lineWidth = 4;
        this.ctx.beginPath();
        this.ctx.arc(x, y, r, 0, 2 * Math.PI);
        this.ctx.fill();
        this.ctx.stroke();
        this.ctx.closePath();
    }

    private drawRectangle(x: number, y: number, w: number, h: number){
        this.ctx.lineWidth = 2;
        this.ctx.beginPath();
        this.ctx.rect(x,y,w,h);
        this.ctx.fill();
        this.ctx.stroke();
        this.ctx.closePath();
    }

    private drawTriangle(p1x: number, p1y: number, p2x: number, p2y: number, p3x: number, p3y: number,){
        this.ctx.lineWidth = 2;
        this.ctx.beginPath();
        this.ctx.moveTo(p1x,p1y);
        this.ctx.lineTo(p2x,p2y);
        this.ctx.lineTo(p3x,p3y);
        this.ctx.lineTo(p1x,p1y);
        this.ctx.fill();
        this.ctx.stroke();
        this.ctx.closePath();
    }

    public generate(): void{
        this.rarity = this.getRarity();
        this.effect = this.getEffect();

        let num: number;
        let item = document.getElementById("items") as HTMLSelectElement;
        if(+item.value === -1) num = Math.floor(Math.random() * this.itemTypes.length);
        else num = +item.value;
        this.itemType = this.itemTypes[num];
        

        let text = document.getElementById("itemDescription");
        text.innerText = "Name: " + this.rarity.rarity + " " + this.itemType.name + " of " + this.effect.name
        + "\nDescription: This " + this.rarity.rarity + " " + this.itemType.name +" " + this.effect.description
        + "\nEffect: " + this.effect.effect(this.rarity.modifier);

        this.ctx.clearRect(0,0,this.width,this.height);
        
        switch(this.itemType.name){
            case "sword": this.drawSword();
            break;
            case "ring": this.drawRing();
            break;
            case "amulet": this.drawAmulet();
            break;
            case "shield": this.drawShield();
            break;
        }

    }

    
}