const {app, BrowserWindow} = require('electron');
const url = require('url');
const path = require('path');

let mainWindow;

function createWindow(){
    mainWindow = new BrowserWindow({
        width: 700,
        height: 500,
        webPreferences: {
            nodeIntegration: true
        }
    });

    mainWindow.loadURL(
        url.format({
            pathname: path.join(__dirname, '/dist/index.html'),
            protocol: "file:",
            slashes: true
        })
    );

    mainWindow.on('close', () => {
        mainWindow = null;
    });

    mainWindow.maximize();
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if(process.platform !== 'darwin'){
        app.quit()
    }
})

app.on('activate', () => {
    if(mainWindow === null){
        createWindow();
    }
})