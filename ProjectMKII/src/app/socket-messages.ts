export const SocketMessage = {
    Welcome: 'welcome to CS 4350 chat!',
    Disconnected: 'You have been disconnected.',
    Reconnected: 'You have been reconnected.',
    UnableToReconnect: 'Attempt to reconnect has failed.'
};