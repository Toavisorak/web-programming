import { Component, ViewChild, ElementRef, HostListener, ViewChildren } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild('canvas', {static: true}) public canvas: ElementRef<HTMLCanvasElement>;
  @ViewChild('generateItem', {static: true}) public generateItem: ElementRef<HTMLInputElement>;
  @ViewChild('itemDescription',{static: true}) public itemDescription: ElementRef<HTMLDivElement>;
  @ViewChild('rarityOptions', {static: true}) public rarityOptions: ElementRef<HTMLSelectElement>;
  @ViewChild('effectOptions', {static: true}) public effectOptions: ElementRef<HTMLSelectElement>;
  @ViewChild('itemOptions', {static: true}) public itemOptions: ElementRef<HTMLSelectElement>;
  @ViewChild('settlement', {static: true}) public settlement: ElementRef<HTMLDivElement>;
  @ViewChild('tavern', {static: true}) public tavern: ElementRef<HTMLHeadingElement>;
  @ViewChild('character', {static: true}) public character: ElementRef<HTMLHeadingElement>;
  @ViewChild('generateSettlement', {static: true}) public generateSettlement: ElementRef<HTMLInputElement>;
  @ViewChild('generateTavern', {static: true}) public generateTavern: ElementRef<HTMLInputElement>;
  @ViewChild('generateCharacter', {static: true}) public generateCharacter: ElementRef<HTMLInputElement>;

  public message: string;

  constructor(){
  }

  private ctx: CanvasRenderingContext2D;
  private centerX: number;
  private centerY: number;
  private currentX: number;
  private currentY: number;

  private fillColor = "black";
  private stroke = 2;
  private width: number;
  private height: number;
  private rarity: Rarity;
  private effect: Effect;
  private scale: number;
  private itemType: string;
  private optionsGenerated = false;

  private effects = [ 
      new Effect("Vitality","affects the health of whoever is using it","#990000",1,"health points per level"), 
      new Effect("Strength", "affects the strength of whoever is using it ", "#ff9933", 1, "strength modifier"),
      new Effect("Speed", "affects the speed of whoever is using it", "lime", 3, "movement speed"),
      new Effect("Holding","can be used to open an extradimensional space to store items in","tan",0,"A storage dimension that can hold 2 cubic meters of stuff"),
      new Effect("Water Breathing", "grants the user the ability to breath underwater", "blue", 0, "Water Breathing"),
      new Effect("Defense", "magically affects the user's defensive ability", "silver", 1, "to your Armor Class"),
      new Effect("Shielding", "provides a temporary shield that affects the users health in battle", "teal", 1, "temporary hitpoints per player level in combat")
  ];

  private rarities = [
      new Rarity("Legendary", "#ffe866", 4),
      new Rarity("Golden", "gold", 3),
      new Rarity("Epic", "violet", 3),
      new Rarity("Lucky", "#b3ffd9", 2),
      new Rarity("Rare", "#9999ff", 2),
      new Rarity("Common", "whitesmoke", 1),
      new Rarity("Mundane", "#cccccc", 1),
      new Rarity("Silver", "silver", 2),
      new Rarity("Wooden", "#ffcc99", -1),
      new Rarity("Broken", "#ffffcc", -2),
      new Rarity("Old", "gray", 1),
      new Rarity("Cursed", "#666699", -3),
      new Rarity("Evil", "#262626", -4)
  ];

  private itemTypes = [
      "sword",
      "ring",
      "amulet",
      "shield"
  ];


  public generateItems(): void{
    this.ctx = this.canvas.nativeElement.getContext("2d");
    this.width = this.canvas.nativeElement.width;
    this.height = this.canvas.nativeElement.height;
    this.centerX = this.width / 2;
    this.centerY = this.height / 2;
    this.currentX = this.centerX;
    this.currentY = this.centerY;
    this.ctx.strokeStyle = this.fillColor;
    this.ctx.lineWidth = this.stroke;

    if(!this.optionsGenerated) this.generateOptions();

    this.rarity = this.getRarity();
    this.effect = this.getEffect();

    let num: number;
    let item = this.itemOptions.nativeElement;
    if(+item.value === -1) num = Math.floor(Math.random() * this.itemTypes.length);
    else num = +item.value;
    this.itemType = this.itemTypes[num];

    this.itemDescription.nativeElement.innerText = "Name: " + this.rarity.rarity + " " + this.itemType + " of " + this.effect.name
    + "\nDescription: This " + this.rarity.rarity + " " + this.itemType +" " + this.effect.description
    + "\nEffect: " + this.effect.effect(this.rarity.modifier);

    this.ctx.clearRect(0,0,this.width,this.height);
    
    switch(this.itemType){
        case "sword": this.drawSword();
        break;
        case "ring": this.drawRing();
        break;
        case "amulet": this.drawAmulet();
        break;
        case "shield": this.drawShield();
        break;
    }

  }

  private generateOptions(){
      this.optionsGenerated = true;

      for(let i = 0; i < this.rarities.length; i++){
          let temp = "<option value='" + i +"'>" + this.rarities[i].rarity + "</option>";
          this.rarityOptions.nativeElement.innerHTML += temp;
      }

      for(let i = 0; i < this.effects.length; i++){
          let temp = "<option value='" + i +"'>" + this.effects[i].name + "</option>";
          this.effectOptions.nativeElement.innerHTML += temp;
      }

      for(let i = 0; i < this.itemTypes.length; i++){
          let temp = "<option value='" + i +"'>" + this.itemTypes[i] + "</option>";
          this.itemOptions.nativeElement.innerHTML += temp;
      }
  }

  public getRarity(): Rarity{
      let rarity: Rarity;

      let num: number;
      let which = this.rarityOptions.nativeElement;
      if(+which.value === -1) num = Math.floor(Math.random() * this.rarities.length)
      else num = +which.value;
      rarity = this.rarities[num];

      return rarity;
  }

  public getEffect(): Effect{
      let effect: Effect;
      
      let num: number;
      let which = this.effectOptions.nativeElement;
      if(+which.value === -1) num = Math.floor(Math.random() * this.effects.length)
      else num = +which.value;
      effect = this.effects[num];
      return effect;
  }


  private drawAmulet(){
      this.scale = this.height / 10;
      this.ctx.lineWidth = 2;
      
      this.currentX = this.centerX;
      this.currentY = this.centerY;

      //Amulet body
      this.ctx.fillStyle = this.rarity.color;
      this.drawCircle(this.centerX, this.centerY, this.scale);

      //Amulet gem
      this.ctx.fillStyle = this.effect.color;
      this.drawCircle(this.centerX, this.centerY, this.scale / 2);

      //Amulet chord
      this.ctx.lineWidth = 2;
      this.ctx.beginPath();
      this.ctx.moveTo(this.centerX, this.centerY - this.scale);
      this.ctx.quadraticCurveTo(this.centerX + this.scale, this.centerY -this.scale * 4, this.centerX, this.centerY - this.scale * 4);
      this.ctx.stroke();
      this.ctx.beginPath();
      this.ctx.moveTo(this.centerX, this.centerY - this.scale);
      this.ctx.quadraticCurveTo(this.centerX - this.scale, this.centerY -this.scale * 4, this.centerX, this.centerY - this.scale * 4);
      this.ctx.stroke();

  }

  private drawSword(){
      this.scale = this.height / 10;
      this.ctx.lineWidth = 2;

      this.currentX = this.centerX;
      this.currentY = this.height - this.scale;

      this.currentX += this.scale;
      this.currentY -= this.scale;

      this.ctx.strokeStyle = this.fillColor;
      this.fillColor = "black";

      //Handle
      this.drawLine(this.centerX + this.scale / 4,this.height - this.scale * 2,this.centerX + this.scale / 4,this.currentY);
      this.drawLine(this.centerX - this.scale / 4,this.height - this.scale * 2,this.centerX - this.scale / 4,this.currentY);
      this.ctx.fillStyle = this.rarity.color;
      this.drawRectangle(this.centerX - this.scale / 4, this.height - this.scale * 2 - this.scale, this.scale / 2, this.height - this.currentY);

      //Main Blade
      this.fillColor = "black";
      this.drawLine(this.centerX + this.scale,this.currentY -= this.scale,this.centerX - this.scale,this.currentY);
      this.drawLine(this.centerX + this.scale / 2,this.currentY,this.centerX + this.scale / 2,this.currentY - this.scale * 5);
      this.drawLine(this.centerX - this.scale / 2,this.currentY,this.centerX - this.scale / 2,this.currentY -= this.scale * 5);
      this.ctx.fillStyle = this.rarity.color;
      this.drawRectangle(this.centerX - this.scale / 2, this.currentY, this.scale, this.scale * 5);

      //Sword Tip
      this.ctx.strokeStyle = this.rarity.color;
      this.drawTriangle(this.centerX - this.scale / 2, this.currentY, this.centerX, this.currentY - this.scale, this.centerX + this.scale / 2, this.currentY);
      this.ctx.strokeStyle = "black";
      this.drawLine(this.centerX - this.scale / 2,this.currentY,this.centerX,this.currentY - this.scale);
      this.drawLine(this.centerX + this.scale / 2,this.currentY,this.centerX,this.currentY -= this.scale);
      this.drawLine(this.centerX,this.height - this.scale * 3, this.centerX, this.currentY);

      //Hilt
      this.ctx.fillStyle = this.effect.color;
      this.drawCircle(this.centerX, this.height - this.scale, this.scale / 2);
  }

  private drawRing(){
      this.scale = this.height / 10;
      this.ctx.lineWidth = 2;
      
      this.fillColor = "black";
      this.ctx.strokeStyle = this.fillColor;

      //Outer ring
      this.ctx.fillStyle = this.rarity.color;
      this.drawCircle(this.centerX, this.centerY, this.scale);

      //InnerRing
      this.ctx.fillStyle = "white";
      this.drawCircle(this.centerX, this.centerY, this.scale / 2);

      //Gem
      this.ctx.fillStyle = this.effect.color;
      this.drawCircle(this.centerX, this.centerY - this.scale, this.scale / 2);
  }

  private drawShield(){
      this.scale = this.height / 10;
      this.ctx.lineWidth = 2;
      
      this.fillColor = "black";
      this.ctx.strokeStyle = this.fillColor;

      //Outer shape
      this.ctx.fillStyle = this.rarity.color;
      this.ctx.lineWidth = 4;
      this.ctx.beginPath();
      this.ctx.moveTo(this.centerX, this.centerY - this.scale * 2)
      this.ctx.quadraticCurveTo(this.centerX + this.scale / 3, this.centerY - this.scale / 3,this.centerX + this.scale * 2,this.centerY - this.scale);
      this.ctx.lineTo(this.centerX, this.centerY + this.scale *3);
      this.ctx.lineTo(this.centerX - this.scale * 2, this.centerY - this.scale);
      this.ctx.quadraticCurveTo(this.centerX - this.scale / 3, this.centerY - this.scale / 3, this.centerX, this.centerY - this.scale * 2);
      this.ctx.fill();
      this.ctx.stroke();
      this.ctx.closePath();

      //Gems
      this.ctx.fillStyle = this.effect.color;
      this.drawCircle(this.centerX, this.centerY, this.scale / 4);
      this.drawCircle(this.centerX + this.scale, this.centerY - this.scale / 3, this.scale / 5);
      this.drawCircle(this.centerX - this.scale, this.centerY - this.scale / 3, this.scale / 5);

  }

  private drawLine(startX: number, startY: number, endX: number, endY: number): void{
      this.ctx.beginPath();
      this.ctx.moveTo(startX,startY);
      this.ctx.lineTo(endX,endY);
      this.ctx.stroke();
      this.ctx.closePath();
  }

  private drawCircle(x: number, y: number, r: number){
      this.ctx.lineWidth = 4;
      this.ctx.beginPath();
      this.ctx.arc(x, y, r, 0, 2 * Math.PI);
      this.ctx.fill();
      this.ctx.stroke();
      this.ctx.closePath();
  }

  private drawRectangle(x: number, y: number, w: number, h: number){
      this.ctx.lineWidth = 2;
      this.ctx.beginPath();
      this.ctx.rect(x,y,w,h);
      this.ctx.fill();
      this.ctx.stroke();
      this.ctx.closePath();
  }

  private drawTriangle(p1x: number, p1y: number, p2x: number, p2y: number, p3x: number, p3y: number,){
      this.ctx.lineWidth = 2;
      this.ctx.beginPath();
      this.ctx.moveTo(p1x,p1y);
      this.ctx.lineTo(p2x,p2y);
      this.ctx.lineTo(p3x,p3y);
      this.ctx.lineTo(p1x,p1y);
      this.ctx.fill();
      this.ctx.stroke();
      this.ctx.closePath();
  }

    private firstHalf: string;
    private secondHalf: string;
    private firstHalves = ["Laughing", "Dancing", "Red", "Leaky", "Shady", "Lost", "Speedy", "Bannered"];
    private secondHalves = ["Herring", "Huntsman", "Tap", "Stag", "Boot", "Mare", "Shield", "Knight", "Hydra"];

    public generateTaverns(){
        this.firstHalf = this.firstHalves[Math.floor(Math.random() * this.firstHalves.length)];
        this.secondHalf = this.secondHalves[Math.floor(Math.random() * this.secondHalves.length)]
        let text = this.tavern.nativeElement;
        text.innerText = "The " + this.firstHalf + " " + this.secondHalf;
    }


    public generateCharacters(){
        let text = this.character.nativeElement;
        text.innerText = this.getName();
    }

    public getName(): string{   
        let firstName: string;
        let secondName: string;
        let lastName: string;
        let firstNames = ["St", "Jo", "Ge", "Je", "S", "M", "H", "Al", "K"];
        let secondNames = ["ev", "ann", "off", "a", "ar", "ega", "ar", "e", "or"];
        let lastNames = ["en", "a", "ry", "m", "ah", "tron", "son", "n", "r"];
    
        firstName = firstNames[Math.floor(Math.random() * firstNames.length)];
        secondName = secondNames[Math.floor(Math.random() * secondNames.length)];
        lastName = lastNames[Math.floor(Math.random() * lastNames.length)];

        return firstName + secondName + lastName;
    }
        
    private exports = [ 
            "food and agriculture", 
            "education opportunities",
            "advancments in technology", 
            "mining and ore refinement", 
            "toolsmiths and blacksmiths", 
            "luxury goods",
            "dangerous wildlife"
        ];
    private types = [
            "Hamlet", 
            "Village", 
            "City", 
            "Town", 
            "Tavern"
        ];
    private sizes = [   
            "near abandoned",
            "small", 
            "large", 
            "massive",
            "monumental"
        ];
    private politics = [
                "peaceful period of growth and plenty.", 
                "chaotic upheaval as the leader has been overthrown by ursurpers and it is unclear who will rise to power.", 
                "tense period of martial law as it is occupied by an army that recently invaded.",
                "plague that has crippled the people of the area.",
                "rise in monster attackes in the surrounding area.",
                "period of economic strife as the main source of work is in jeapordy.",
                "famine that threatens the survival of those who live here."
            ];

    private output: string;
    private type: string;
    private size: number;
    private politicalState: string;

    public generateSettlements(){
        this.size = Math.floor(Math.random() * this.sizes.length);
        this.output = this.getExports(this.size);
        this.type = this.types[Math.floor(Math.random() * this.types.length)];
        this.politicalState = this.politics[Math.floor(Math.random() * this.politics.length)];
        let text = this.settlement.nativeElement;
        let temp = "The " + this.sizes[this.size] + " " + this.type + " is known for its " + this.output +
                    ". It is currently experiencing a " + this.politicalState + "\n";

        temp += "<ul>" + "<li>Town Leader: " + this.getName() + "</li>";
        for(let i = 0; i < this.size; i++){
            temp += "<li>Other: " + this.getName() + "</li>";
        }
        temp += "</ul>"
        text.innerHTML = temp;
    }

    private getExports(size: number): string{
        let output = "";
        let num: number;

        let used: boolean[] = [];

        for(let i = 0; i < this.exports.length; i++){
            used.push(false);
        }

        for(let i = 0; i < size; i++){
            num = Math.floor(Math.random() * this.exports.length);
            if(!used[num]){
                output += this.exports[num] + ", ";
                used[num] = true;
            }
        }
        if(size > 0) output += "and ";

        do{
            num = Math.floor(Math.random() * this.exports.length);
            if(!used[num]){
                output += this.exports[num];
                break;
            }
        }while(true);
        return output;
    }
}

class Effect {
  public name: string;
  public description: string;
  public color: string;
  private effectType: number;
  private effectDescription: string;

  constructor(name: string, description: string, color: string, effectType: number, effectDescription: string){
      this.name = name;
      this.description = description;
      this.color = color;
      this.effectType = effectType;
      this.effectDescription = effectDescription;
  }

  public effect(amount: number): string{
      let effect: string;

      //1. Additive
      //2. Multiplicative
      //3. Additive x10
      //4. Multiplicative x10
      //0. Other (no number)

      switch(this.effectType){
          case 1: effect = ((amount > 0) ? "+" : "")  + amount + " " + this.effectDescription;
                  break;
          case 2: effect = "x" + amount + " " + this.effectDescription;
                  break;
          case 3: effect = ((amount > 0) ? "+" : "") + amount * 10 + " " + this.effectDescription;
                   break;
          case 4: effect = "x" + amount * 10 + " " + this.effectDescription;
                  break;
          case 0: effect = this.effectDescription + (amount >= 0? "" : ". This item is also possesed by an entity that may or may not be hostile.");
                  break;
          default: effect = "";
      }

      return effect;
  }
}

class Rarity {
  public rarity: string;
  public color: string;
  public modifier: number;

  constructor(rarity: string, color: string, modifier: number){
      this.rarity = rarity;
      this.color = color;
      this.modifier = modifier;
  }
}

