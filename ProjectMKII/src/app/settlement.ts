

export class Settlement{
    private exports = [ 
                        "food and agriculture", 
                        "education opportunities",
                        "advancments in technology", 
                        "mining and ore refinement", 
                        "toolsmiths and blacksmiths", 
                        "luxury goods",
                        "dangerous wildlife"
                    ];
    private types = [
                        "Hamlet", 
                        "Village", 
                        "City", 
                        "Town", 
                        "Tavern"
                    ];
    private sizes = [   
                        "near abandoned",
                        "small", 
                        "large", 
                        "massive",
                        "monumental"
                    ];
    private politics = [
                            "peaceful period of growth and plenty.", 
                            "chaotic upheaval as the leader has been overthrown by ursurpers and it is unclear who will rise to power.", 
                            "tense period of martial law as it is occupied by an army that recently invaded."
                        ];

    private output: string;
    private type: string;
    private size: number;
    private politicalState: string;

    public generate(){
        this.size = Math.floor(Math.random() * this.sizes.length);
        this.output = this.getExports(this.size);
        this.type = this.types[Math.floor(Math.random() * this.types.length)];
        this.politicalState = this.politics[Math.floor(Math.random() * this.politics.length)];
        let text = document.getElementById("settlementDescription");
        text.innerText = "The " + this.sizes[this.size] + " " + this.type + " is known for its " + this.output 
                        + ". It is currently experiencing a " + this.politicalState;
    }

    private getExports(size: number): string{
        let output = "";
        let num: number;

        let used: boolean[] = [];

        for(let i = 0; i < this.exports.length; i++){
            used.push(false);
        }

        for(let i = 0; i < size; i++){
            num = Math.floor(Math.random() * this.exports.length);
            if(!used[num]){
                output += this.exports[num] + ", ";
                used[num] = true;
            }
        }
        if(size > 0) output += "and ";

        do{
            num = Math.floor(Math.random() * this.exports.length);
            if(!used[num]){
                output += this.exports[num];
                used[num] = true;
            }
        }while(!used[num]);
        return output;
    }
}

export class Tavern{
    private firstHalf: string;
    private secondHalf: string;
    private firstHalves = ["Laughing", "Dancing", "Red", "Leaky", "Shady", "Lost", "Speedy", "Bannered"];
    private secondHalves = ["Herring", "Huntsman", "Tap", "Stag", "Boot", "Mare", "Shield", "Knight", "Hydra"];

    public generateTaverns(){
        this.firstHalf = this.firstHalves[Math.floor(Math.random() * this.firstHalves.length)];
        this.secondHalf = this.secondHalves[Math.floor(Math.random() * this.secondHalves.length)]
        let text = document.getElementById("tavernName");
        text.innerText = "The " + this.firstHalf + " " + this.secondHalf;
    }
}

export class Character{
    private firstName: string;
    private secondName: string;
    private lastName: string;
    private firstNames = ["St", "Jo", "Ge", "Je", "S", "M", "H", "Al", "K"];
    private secondNames = ["ev", "ann", "off", "a", "ar", "ega", "ar", "e", "or"];
    private lastNames = ["en", "a", "ry", "m", "ah", "tron", "son", "n", "r"];

    public generate(){
        this.firstName = this.firstNames[Math.floor(Math.random() * this.firstNames.length)];
        this.secondName = this.secondNames[Math.floor(Math.random() * this.secondNames.length)];
        this.lastName = this.lastNames[Math.floor(Math.random() * this.lastNames.length)];
        let text = document.getElementById("characterName");
        text.innerText = this.firstName + this.secondName + this.lastName;
    }
}